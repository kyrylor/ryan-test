### Short guide to make it work

- Install Docker
- Create `.env.local` file, or use fill existing `.env`
  - `DATABASE_URL` – database connection string 
  - `ADMIN_EMAIL` – admin email
  - `ADMIN_PASSWORD` – admin password
- Open terminal in the project folder
- Run `sh rundev.sh` inside the project folder

That's all!

### Available users

Email | Password | Role | Nickname
--- | --- | --- | ---
`ADMIN_EMAIL` | `ADMIN_PASSWORD` | ROLE_ADMIN | admin 
kyrylo@test.com | User_11 | ROLE_USER | kyrylo
someone@test.com | User_22 | ROLE_ADMIN | someone