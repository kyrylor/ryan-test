import Modal from "bootstrap/js/src/modal"
import axios from "axios"

class ConfirmDeleteModal {
    PAGE_LIST = "list"
    PAGE_DETAIL = "detail"

    constructor(deleteButtonSelector, toastHandler, modalElement, cardSelector){
        this.deleteButtonSelector = deleteButtonSelector
        this.toastHandler = toastHandler
        this.modalElement = modalElement
        this.cardSelector = cardSelector

        this.modal = new Modal(modalElement)
        this.confirmDeleteButton = modalElement.querySelector('#confirmPostDelete')
        this.initialHtml = modalElement.querySelector('#confirmPostDelete').innerHTML
        this.progressHtml = '<span class="spinner-grow spinner-grow-sm" role="status"></span> Deleting...'
        this.setPage(modalElement.getAttribute('data-page'));

        this.init()
    }

    init(){
        const deleteButtons = document.querySelectorAll(this.deleteButtonSelector)
        deleteButtons.forEach(deleteButton => {
            deleteButton.addEventListener('click', event => {
                const deleteButton = event.target
                const postId = deleteButton.getAttribute('data-post-id')
                const deletePath = deleteButton.getAttribute('data-delete-path')

                this.showModal(postId, deletePath)
            })
        })

        this.confirmDeleteButton.addEventListener('click', event => {
            const confirmDeleteButton = event.target
            const postId = confirmDeleteButton.getAttribute('data-post-id')
            const deletePath = confirmDeleteButton.getAttribute('data-delete-path')

            this.confirm(postId, deletePath)
        })
    }

    setPage(page){
        switch(page){
            case 'all':
                this.page = 'list'
                break
            case 'my':
                this.page = 'list'
                break
            case 'post':
                this.page = 'detail'
                break
        }
    }

    showModal(postId, deleteUrl){
        const modalTitle = this.modalElement.querySelector('.modal-title')
        modalTitle.textContent = 'Are you sure you want to delete the post #' + postId + '?'
        this.confirmDeleteButton.setAttribute('data-post-id', postId)
        this.confirmDeleteButton.setAttribute('data-delete-path', deleteUrl)

        this.modal.show()
    }

    hideModal(){
        this.modal.hide()
    }

    confirm(postId, deletePath){
        this.disableConfirmButton()
        this.sendDeleteAjax(postId, deletePath)
    }

    sendDeleteAjax(postId, deletePath){
        axios.delete(deletePath)
            .then((response) => {
                this.sendSuccessToast('You\'ve successfully deleted the post #' + postId)
                if(this.page === this.PAGE_LIST) {
                    this.deleteCard(postId)
                }
            })
            .catch((error) => {
                let toastMessage
                if(error.response){
                    toastMessage = error.response.statusText
                } else {
                    toastMessage = 'Internal error occurred. Try again later.'
                    console.log(error);
                }

                this.sendErrorToast(toastMessage)
            })
            .then(() => {
                this.enableConfirmButton()
                this.closeModal()
                if(this.page === this.PAGE_DETAIL){
                    setTimeout(() => {
                        this.redirect(this.modalElement.getAttribute('data-referer'))
                    }, 3000)
                }
            });
    }

    disableConfirmButton(){
        this.confirmDeleteButton.disabled = true
        this.confirmDeleteButton.innerHTML = this.progressHtml
    }

    enableConfirmButton(){
        this.confirmDeleteButton.disabled = false
        this.confirmDeleteButton.innerHTML = this.initialHtml
    }

    closeModal(){
        this.modal.hide()
    }

    sendSuccessToast(message){
        this.toastHandler.append('success', message)
    }

    sendErrorToast(message){
        this.toastHandler.append('danger', message)
    }

    redirect(url){
        document.location.href = url;
    }

    deleteCard(postId){
        const cardToDelete = document.querySelector(this.cardSelector + '[data-post-id="' + postId + '"]')

        cardToDelete.classList.add('removed');
        setTimeout(() => {
            cardToDelete.remove()
        }, 500)
    }
}

export default ConfirmDeleteModal
