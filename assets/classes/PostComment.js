import axios from "axios";

class PostComment {
    constructor(commentsContainerSelector, templateSelector, leaveCommentFormSelector, toastHandler) {
        window.exps = document.querySelector(templateSelector)
        this.commentsContainer = document.querySelector(commentsContainerSelector)
        this.template = document.querySelector(templateSelector)
            .content.querySelector('div');
        this.form = document.querySelector(leaveCommentFormSelector)
        this.toastHandler = toastHandler

        this.init()
    }

    init(){
        this.form.addEventListener('submit', (event) => {
            event.preventDefault()

            const formData = new FormData(event.target)
            this.sendCommentAjax(formData)
        });

        console.log('Initialized');
    }

    sendCommentAjax(formData){
        axios
            .post(this.form.getAttribute('action'), formData)
            .then((response) => {
                this.toastHandler.append('success', 'Your comment is posted')
                console.log(response.data)
                this.prependComment(response.data.name, response.data.comment, response.data.user)
            })
            .catch((error) => {
                try {
                    const errorMessages = error.response.data.errors
                    for(let key in errorMessages){
                        this.toastHandler.append('danger', errorMessages[key])
                    }
                } catch (e) {
                    console.log(e)
                }
            });
    }

    prependComment(name, comment, user){
        const nickname = user === null ? null : user.nickname
        let nicknameSting = nickname !== null
            ? '(<a href="/posts/' + nickname  + '">@' + nickname + '</a>)'
            : '';

        let commentCard = this.template.cloneNode(true)

        commentCard.querySelector('.comment-author')
            .innerHTML = '<b>' + name + '</b> ' + nicknameSting
        commentCard.querySelector('.comment-created').textContent = 'A few moments ago'
        commentCard.querySelector('.card-body').textContent = comment

        this.commentsContainer.prepend(commentCard)
        this.increaseCommentCounter()
        commentCard.classList.add('new')

        this.form.reset()
    }

    increaseCommentCounter(){
        document.getElementById('commentNumber').innerHTML++;
    }
}

export default PostComment