import {Toast} from "bootstrap";

class ToastHandler {
    constructor(delay, toastSelector, toastContainer, templateSelector) {
        this.delay = delay
        this.toastSelector = toastSelector
        this.toastContainer = toastContainer
        this.templateSelector = templateSelector
    }

    dispatchAll(){
        let toastElList = [].slice.call(document.querySelectorAll(this.toastSelector))
        toastElList.map(function (toastEl) {
            new Toast(toastEl, { delay: this.delay }).show()

            toastEl.addEventListener('hidden.bs.toast', function (event) {
                event.target.remove()
            })
        }.bind(this))
    }

    append(type, message){
        const toastTemplate = document.querySelector(this.templateSelector);
        let toast = toastTemplate.cloneNode(true)

        toast.classList.remove(this.templateSelector.substring(1));
        toast.classList.add(this.toastSelector.substring(1), 'bg-' + type);
        toast.querySelector('.toast-body').textContent = message;

        this.toastContainer.appendChild(toast)
        new Toast(toast, { delay: this.delay }).show()
    }
}

export default ToastHandler;