import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import ToastHandler from "./classes/ToastHandler";
import ConfirmDeleteModal from "./classes/ConfirmDeleteModal";
import PostComment from "./classes/PostComment";

window.addEventListener('load', function(){
    const toastHandler = new ToastHandler(
        3000,
        '.toast-message',
        document.querySelector('.toast-container'),
        '.toast-template'
    );
    toastHandler.dispatchAll()

    const deletePostModal = document.getElementById('deletePostModal')
    if(deletePostModal !== null){
        let modalHandler = new ConfirmDeleteModal(
            '.delete-post-button',
            toastHandler,
            deletePostModal,
            '.card'
        )
    }

    const commentForm = document.getElementById('leaveComment')
    if(commentForm !== null){
        let commentHandler = new PostComment(
            '.comments-container',
            'template#commentCard',
            '#leaveComment',
            toastHandler
        )
    }
})