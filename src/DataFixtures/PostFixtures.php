<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{

    private array $lorem = [
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    ];

    public function load(ObjectManager $manager): void
    {
        $postsNb = rand(30, 100);

        for($i = 0; $i < $postsNb; $i++){
            $post = (new Post())
                ->setAuthor($this->getRandomPostAuthor($manager))
                ->setContent($this->getRandomPostContent())
                ->setExcerpt($this->getRandomPostExcerpt())
                ->setTitle($this->getRandomPostTitle());

            $manager->persist($post);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }

    public function getRandomPostAuthor(ObjectManager $manager): User
    {
        $users = $manager->getRepository(User::class)->findAll();

        return $users[array_rand($users)];
    }

    public function getRandomPostExcerpt()
    {
        return $this->lorem[array_rand($this->lorem)];
    }

    public function getRandomPostContent()
    {
        $postContentParts = [];
        $partsNb = rand(2, 10);

        for($i = 0; $i < $partsNb; $i++){
            $postContentParts[] = $this->lorem[array_rand($this->lorem)];
        }

        return implode(' ', $postContentParts);
    }

    public function getRandomPostTitle()
    {
        $titleParts = [];
        $partsNb = rand(1, 4);
        $randomWords = explode(' ', $this->lorem[array_rand($this->lorem)]);

        for($i = 0; $i < $partsNb; $i++){
            $titleParts[] = preg_replace('/(,|\.)/', '', $randomWords[array_rand($randomWords)]);
        }

        return ucfirst(strtolower(implode(' ', $titleParts)));
    }
}