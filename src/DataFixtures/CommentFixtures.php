<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\PostComment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    private array $names = [
        'Jim',
        'Lily',
        'Mike',
        'Mohamad',
        'Roman',
        'Jessica'
    ];

    private array $surnames = [
        'Gates',
        'Jobs',
        'Zuckerberg',
        'Musk',
        'Williams',
        'Armstrong'
    ];

    private array $lorem = [
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    ];

    public function load(ObjectManager $manager): void
    {
        $commentsNb = rand(200, 500);

        for($i = 0; $i < $commentsNb; $i++){
            $comment = (new PostComment())
                ->setUser($this->getRandomCommenter($manager))
                ->setName($this->getRandomName())
                ->setPost($this->getRandomPost($manager))
                ->setComment($this->getRandomComment());

            $manager->persist($comment);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            PostFixtures::class
        ];
    }

    public function getRandomCommenter(ObjectManager $manager): ?User
    {
        $users = $manager->getRepository(User::class)->findAll();
        $users[] = null;

        return $users[array_rand($users)];
    }

    public function getRandomPost(ObjectManager $manager): Post
    {
        $posts = $manager->getRepository(Post::class)->findAll();

        return $posts[array_rand($posts)];
    }

    public function getRandomComment()
    {
        return $this->lorem[array_rand($this->lorem)];
    }

    public function getRandomName()
    {
        return $this->names[array_rand($this->names)] . ' ' . $this->surnames[array_rand($this->surnames)];
    }
}