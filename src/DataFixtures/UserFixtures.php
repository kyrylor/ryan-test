<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    private ContainerInterface $container;
    private UserPasswordHasherInterface $passwordHasher;

    public function load(ObjectManager $manager): void
    {
        $users = [
            [
                'email'    => $this->container->getParameter('admin.email'),
                'password' => $this->container->getParameter('admin.password'),
                'nickname' => 'admin',
                'roles'    => ['ROLE_ADMIN']
            ],
            [
                'email'    => 'kyrylo@test.com',
                'password' => 'User_11',
                'nickname' => 'kyrylo',
                'roles'    => ['ROLE_USER']
            ],
            [
                'email'    => 'someone@test.com',
                'password' => 'User_22',
                'nickname' => 'someone',
                'roles'    => ['ROLE_ADMIN']
            ]
        ];

        foreach($users as $user){
            $this->loadUser(
                $manager,
                $user['email'],
                $user['nickname'],
                $user['password'],
                $user['roles']
            );
        }
    }

    public function loadUser(
        ObjectManager $manager,
        string $email,
        string $nickname,
        string $password,
        array $roles
    ): void
    {
        $user = (new User())
            ->setEmail($email)
            ->setNickname($nickname)
            ->setRoles($roles)
        ;

        $plainPassword = $password;
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $plainPassword
        );

        $user->setPassword($hashedPassword);

        $manager->persist($user);
        $manager->flush();
    }

    public function __construct(ContainerInterface $container, UserPasswordHasherInterface $passwordHasher)
    {
        $this->container = $container;
        $this->passwordHasher = $passwordHasher;

    }
}