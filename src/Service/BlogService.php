<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\PostCommentRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogService
{
    private EntityManagerInterface $entityManager;
    private PostRepository $postRepository;
    private PostCommentRepository $postCommentRepository;

    public function getPostsPager(User $author = null, int $page = 1)
    {
        $postsQueryBuilder = $this->postRepository->createPostsQueryBuilder($author);

        $pager = new Pagerfanta(
            new QueryAdapter($postsQueryBuilder)
        );

        try {
            $pager->setCurrentPage($page);
        } catch (\Throwable $exception) {
            throw new NotFoundHttpException("There is no such page.");
        }

        return $pager;
    }

    public function getPost($postId)
    {
        $post = $this->postRepository->find($postId);

        if(is_null($post)){
            throw new NotFoundHttpException('There is no such post.');
        }

        return $post;
    }

    public function getCommentsByPost($post)
    {
        return $this->postCommentRepository->findBy(
            ['post' => $post],
            ['createdAt' => 'DESC']
        );
    }

    public function __construct(
        EntityManagerInterface $entityManager,
        PostRepository $postRepository,
        PostCommentRepository $postCommentRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->postRepository = $postRepository;
        $this->postCommentRepository = $postCommentRepository;
    }
}
