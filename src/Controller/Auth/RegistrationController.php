<?php

namespace App\Controller\Auth;

use App\Entity\User;
use App\Form\Type\Auth\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class RegistrationController extends AbstractController
{
    /**
     * @Route("register", name="register")
     * @Template(template="auth/registration.html.twig")
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordHasher
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function registerAction(
        Request $request,
        UserPasswordHasherInterface $passwordHasher,
        EntityManagerInterface $entityManager
    )
    {
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('feed');
        }

        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $user = $form->getData();

            $password = $passwordHasher->hashPassword(
                $user,
                $user->getPlainPassword()
            );

            $user->setPassword($password);

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', sprintf('Hello, %s! You\'ve successfully registered.', $user->getNickname()));

            return $this->redirectToRoute("login");
        }

        return [
            'form' => $form->createView()
        ];
    }

}