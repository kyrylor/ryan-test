<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\Type\Blog\PostType;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\Service\BlogService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class PostController extends AbstractController
{
    private BlogService $blogService;

    /**
     * @Route("/", name="all")
     * @Template("post/posts.html.twig")
     * @param Request $request
     * @return array
     */
    public function allPostsAction(
        Request $request
    )
    {
        $page = $request->query->get('page', 1);
        $pager = $this->blogService->getPostsPager(null, $page);

        return [
            'pager' => $pager
        ];
    }

    /**
     * @Route("/posts/my", name="my")
     * @Template("post/posts.html.twig")
     * @param UserInterface $user
     * @param Request $request
     * @return array
     */
    public function myPostsAction(
        UserInterface $user,
        Request $request
    )
    {
        $page = $request->query->get('page', 1);
        $pager = $this->blogService->getPostsPager($user, $page);

        return [
            'pager' => $pager
        ];
    }

    /**
     * @Route("/posts/{nickname}", name="postsByAuthor")
     * @Template("post/posts.html.twig")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param string $nickname
     * @return array
     */
    public function postsByAuthorAction(
        Request $request,
        UserRepository $userRepository,
        string $nickname
    )
    {
        $page = $request->query->get('page', 1);
        $user = $userRepository->findOneBy([
            'nickname' => $nickname
        ]);

        if(is_null($user)){
            throw new NotFoundHttpException("The is no user with such ID.");
        }

        $pager = $this->blogService->getPostsPager($user, $page);

        return [
            'author' => $user,
            'pager' => $pager
        ];
    }

    /**
     * @Route("/post/new", name="new")
     * @Template("post/edit.html.twig")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(
        Request $request,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository
    )
    {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var Post $post */
            $post = $form->getData();

            $author = $userRepository->findOneBy([
                'email' => $this->getUser()->getUserIdentifier()
            ]);

            $post->setAuthor($author);

            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash('success', 'Good job! Post #' . $post->getId() . ' successfully created');

            return $this->redirectToRoute("post", [
                'postId' => $post->getId()
            ]);
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/post/edit/{postId}", name="edit")
     * @Template
     * @param int $postId
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param PostRepository $postRepository
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(
        int $postId,
        Request $request,
        EntityManagerInterface $entityManager,
        PostRepository $postRepository
    )
    {
        $post = $postRepository->find($postId);

        if(is_null($post)){
            throw new NotFoundHttpException("There is no post with such ID");
        }

        if(!$this->isGranted('ROLE_ADMIN') && $post->getAuthor() !== $this->getUser()){
            $this->addFlash('danger', 'You don\'t have access to edit this post.');

            return $this->redirectToRoute("post", [
                'postId' => $post->getId()
            ]);
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var Post $post */
            $post = $form->getData();

            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash('success', 'Good job! Post #' . $post->getId() . ' successfully updated');

            return $this->redirectToRoute("post", [
                'postId' => $post->getId()
            ]);
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/post/delete/{postId}", name="delete", methods={"DELETE"})
     * @param int $postId
     * @param EntityManagerInterface $entityManager
     * @param PostRepository $postRepository
     * @return JsonResponse
     */
    public function deleteAction(
        int $postId,
        EntityManagerInterface $entityManager,
        PostRepository $postRepository
    )
    {
        $post = $postRepository->find($postId);
        if(is_null($postId)){
            throw new NotFoundHttpException("There is no post with such ID.");
        }

        if(!$this->isGranted('ROLE_ADMIN') && $post->getAuthor() !== $this->getUser()){
            throw new AccessDeniedHttpException("Access denied.");
        }

        $entityManager->remove($post);
        $entityManager->flush();

        return $this->json($postId);
    }

    /**
     * @Route("/post/{postId}", name="post")
     * @Template
     * @param int $postId
     * @return array
     */
    public function postAction(
        int $postId
    )
    {
        $post = $this->blogService->getPost($postId);

        return [
            'post'     => $post,
            'comments' => $this->blogService->getCommentsByPost($post)
        ];
    }

    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }
}