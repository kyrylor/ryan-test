<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\BlogService;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private BlogService $blogService;

    /**
     * @Route("/users", name="all_users")
     * @Template
     * @param Request $request
     * @param UserRepository $userRepository
     * @return array
     */
    public function allAction(
        Request $request,
        UserRepository $userRepository
    )
    {
        $usersQueryBuilder = $userRepository->createUsersQueryBuilder();
        $page = $request->query->get('page', 1);

        $pager = new Pagerfanta(
            new QueryAdapter($usersQueryBuilder)
        );

        try {
            $pager->setCurrentPage($page);
        } catch (\Throwable $exception) {
            throw new NotFoundHttpException("There is no such page.");
        }

        return [
            'pager' => $pager
        ];
    }


    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }
}