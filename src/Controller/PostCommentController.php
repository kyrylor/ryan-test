<?php

namespace App\Controller;

use App\Entity\PostComment;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Route("/comments")
 */
class PostCommentController extends AbstractController
{
    /**
     * @Route("/post", name="postComment", methods={"POST"})
     * @param Request $request
     * @param PostRepository $postRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function postCommentAction(
        Request $request,
        PostRepository $postRepository,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ): JsonResponse
    {
        $user = $this->getUser();
        $name = $request->request->get('name');
        $comment = $request->request->get('comment');
        $post = $postRepository->find($request->request->get('postId'));

        $postComment = (new PostComment())
            ->setUser($user)
            ->setName($name)
            ->setComment($comment)
            ->setPost($post)
        ;

        $violations = $validator->validate($postComment);

        if($violations->count()){
            $errors = [];

            foreach ($violations as $violation){
                $errors[] = $violation->getMessage();
            }

            return $this->json([
                'errors' => $errors
            ], 500);
        }

        $entityManager->persist($postComment);
        $entityManager->flush();

        return $this->json($postComment);
    }
}